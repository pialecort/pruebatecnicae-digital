package com.phpinfo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class PhpinfoApplication {

	public static void main(String[] args) {
		SpringApplication.run(PhpinfoApplication.class, args);
	}

}
