package com.phpinfo.models.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.phpinfo.models.entity.Venta;

public interface IVentaDao extends CrudRepository<Venta, Long> {
	
	@Query(nativeQuery = true,value = "SELECT DISTINCT(vendedor), fecha, count(producto), sum(monto) as total from ventas group by vendedor, fecha;")
	public List<Venta> findByVendedoryFecha(@Param("filtro")String vend);
	
	//metodo ocupando Query nativa, también se puede ocupando JPQL. 

}
