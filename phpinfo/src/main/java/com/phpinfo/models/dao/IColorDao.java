package com.phpinfo.models.dao;


import org.springframework.data.repository.CrudRepository;

import com.phpinfo.models.entity.Color;

public interface IColorDao extends  CrudRepository<Color, Long> {
	
//También se puede implementar/extender de JPAReposiroty
}
