package com.phpinfo.models.service;

import java.util.List;


import com.phpinfo.models.entity.Color;
import com.phpinfo.models.entity.Venta;

public interface IColorService {

	public List<Color> findAll();
	
	public void save(Color venta);
	
	public Color findOne(Long id);
	
	public void delete(Long id);
}
