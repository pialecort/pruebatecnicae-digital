package com.phpinfo.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.phpinfo.models.dao.IVentaDao;
import com.phpinfo.models.entity.Venta;

@Service
public class VentaServiceImplement implements IVentaService {
	
	//implementación de los servicios de manera general, estos son llamados e implemntados por el controlador 

	@Autowired
	private IVentaDao ventaDao;
	
	@Override
	public List<Venta> findAll() {
		// TODO Auto-generated method stub
		return (List <Venta>) ventaDao.findAll();
	}

	@Override
	public void save(Venta venta) {
		// TODO Auto-generated method stub
		ventaDao.save(venta);
	}

	@Override
	public Venta findOne(Long id) {
		// TODO Auto-generated method stub
		return ventaDao.findById(id).orElse(null);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		ventaDao.deleteById(id);		
	}

	
	//intento de implementación para la logica de filtros por vendedor y fecha.
	@Override
	public List<Venta> listarTotal(String filtro) throws Exception {
		try {
			
			List<Venta> total = ventaDao.findByVendedoryFecha(filtro);
			return total;
			
		}catch(Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	
	

}
