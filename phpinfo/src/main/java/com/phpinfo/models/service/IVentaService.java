package com.phpinfo.models.service;

import java.util.List;


import com.phpinfo.models.entity.Venta;

public interface IVentaService {
	
	//interfaz para los metodos generales
	
	public List<Venta> findAll();
	
	public void save(Venta venta);
	
	public Venta findOne(Long id);
	
	public void delete(Long id);
	
	public List<Venta> listarTotal(String filtro) throws Exception;

}
