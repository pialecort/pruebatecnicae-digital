package com.phpinfo.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.phpinfo.models.dao.IColorDao;
import com.phpinfo.models.entity.Color;

@Service //Anotación para convertir la clase en un compomente de la Interfaz a implementar y que sea reconocida por Spring
public class ColorServiceImplement implements IColorService {
	
	//implementación de los servicios de manera general, estos son llamados e implemntados por el controlador 

	
	@Autowired//Anotación para implementar
	private IColorDao colorDao;
	
	//Listar los colores
	@Override //sobreescribe el método creado en el DAO
	@Transactional(readOnly = true)
	public List<Color> findAll() {
		// TODO Auto-generated method stub
		return (List<Color>) colorDao.findAll();
		
	}

	//Guardar los colores
	@Override
	public void save(Color venta) {
		// TODO Auto-generated method stub
		
	}

	//Buscar color por su ID
	@Override
	public Color findOne(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	//Elimina un color por su ID
	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		
	}

}
