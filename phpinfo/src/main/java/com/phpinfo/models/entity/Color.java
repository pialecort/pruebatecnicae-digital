package com.phpinfo.models.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "color")
public class Color implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY) //Anotación para autoincrimentar el campo. En caso de MySQL se ocupa .Identity
	private Long id;
	
	private String nombre;
	
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	private static final long serialVersionUID = 1L; //Implementación de la interfaz serializable para autoincrementar campo
	
}
