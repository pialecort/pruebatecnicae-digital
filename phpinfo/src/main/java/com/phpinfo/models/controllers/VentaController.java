package com.phpinfo.models.controllers;


import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.phpinfo.models.entity.Venta;
import com.phpinfo.models.service.IVentaService;

@Controller
@SessionAttributes("ventas")
public class VentaController {
	
	@Autowired
	private IVentaService ventaService;
	
	//Logica para listar las ventas, que los lista llamando al método ventaService.findAll creado en la clase de Implementación de Servicio, Interfaz del mismo y el DAO/Repository
	@RequestMapping(value = "/listarventas", method = RequestMethod.GET)
	public String listar(Model model) {
		
		model.addAttribute("titulo", "Listado de ventas");
		model.addAttribute("ventas", ventaService.findAll());
		return "listarventas";
	}
	
	//Lógica para crear un nuevo objeto tipo Venta
	@RequestMapping(value = "/formventas")
	public String crear(Map<String, Object> model) {

		Venta venta = new Venta();
		model.put("venta", venta);
		model.put("titulo", "Formulario de Cliente");
		return "formventas";
	}

}
