package com.phpinfo.models.controllers;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.phpinfo.models.entity.Color;
import com.phpinfo.models.service.IColorService;

@Controller //Anotación para las clases controladoras
@SessionAttributes("color") //Anotación para identificar a qué tabla del esquema corresponde
public class ColorController {
	
	@Autowired
	private IColorService colorService;
	
	//Logica para listar los colores, que los lista llamando al método colorService.findAll creado en la clase de Implementación de Servicio, Interfaz del mismo y el DAO/Repository
	@RequestMapping(value = "/listarcolores", method = RequestMethod.GET)//anotación para indicar a qué vista corresponde y qué metodo (get, put, post, delete, etc) ocupara para la recepción y retorno de datos desde la vista a la base de datos
	public String listar(Model model) {
		model.addAttribute("titulo", "Listado de colores");
		model.addAttribute("colores", colorService.findAll());
		return "listarcolores";
	}
	
	//Lógica para crear un nuevo color, gracias a la creación de un nuevo objeto del tipo Color
	@RequestMapping(value = "/formcolor")
	public String crear(Map<String, Object> model) {

		Color color = new Color();
		model.put("color", color);
		model.put("titulo", "Formulario de colores");
		return "formcolor";
	}
	
	
}
